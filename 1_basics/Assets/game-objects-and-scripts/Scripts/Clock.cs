using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Clock : MonoBehaviour
{
    public Transform hours;
    public Transform minutes;
    public Transform seconds;

    // fitting hours, minutes, seconds in 360 degrees
    private float hourDegreeRatio = -30f;
    private float minuteDegreeRatio = -6f;
    private float secondDegreeRatio = -6f;

    void Update()
    {
        var time = DateTime.Now;
        var timeSmooth = DateTime.Now.TimeOfDay ;
        
        seconds.transform.localRotation = Quaternion.Euler(0, 0, secondDegreeRatio * time.Second);
        minutes.transform.localRotation = Quaternion.Euler(0, 0, minuteDegreeRatio * (float) timeSmooth.TotalMinutes);
        hours.transform.localRotation = Quaternion.Euler(0, 0, hourDegreeRatio * (float) timeSmooth.TotalHours);
    }
}